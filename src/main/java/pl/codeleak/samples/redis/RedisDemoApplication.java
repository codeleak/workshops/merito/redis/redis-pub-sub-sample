package pl.codeleak.samples.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class RedisDemoApplication implements CommandLineRunner {

    @Autowired
    private MessageSubscriber messageSubscriber;

    @Autowired
    private MessagePublisher messagePublisher;

    public static void main(String[] args) {
        SpringApplication.run(RedisDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name:");
        String name = scanner.nextLine();

        messageSubscriber.setUsername(name);

        Thread subscriberThread = new Thread(() -> {
            while (true) {
                // Listening for messages
            }
        });
        subscriberThread.setDaemon(true);
        subscriberThread.start();

        System.out.println("You have joined the chat room. Type your messages below:");

        while (true) {
            String message = scanner.nextLine();
            messagePublisher.publish(name + ": " + message);
        }
    }
}
