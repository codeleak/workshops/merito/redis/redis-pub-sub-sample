package pl.codeleak.samples.redis;

import org.springframework.stereotype.Service;

@Service
public class MessageSubscriber {

    private String username;

    public void setUsername(String username) {
        this.username = username;
    }

    public void onMessage(String message, String channel) {
        if (!message.startsWith(username + ":")) {
            System.out.println(message);
        }
    }
}
